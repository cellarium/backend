# Cellarium : Backend

[![pipeline status](https://gitlab.com/cellarium/backend/badges/master/pipeline.svg)](https://gitlab.com/cellarium/backend/-/commits/master)

![Icon](./icon.png)

## Table Of Contents

- [Cellarium : Backend](#cellarium--backend)
  - [Table Of Contents](#table-of-contents)
  - [Description](#description)
  - [Access](#access)
  - [Documentations](#documentations)
  - [Requirements](#requirements)
  - [Build](#build)
    - [Build : PDM](#build--pdm)
    - [Build : Docker](#build--docker)
  - [Deploy](#deploy)
    - [Deploy : PDM](#deploy--pdm)
    - [Deploy : Docker](#deploy--docker)
  - [Unit Testing](#unit-testing)
    - [Unit Testing : PDM](#unit-testing--pdm)
    - [Unit Testing : Docker](#unit-testing--docker)
  - [Contributors](#contributors)
  - [Licence](#licence)

## Description

**Cellarium** is a web application to manage your food. This is the Backend part.

## Access

- [Cellarium Backend Development (Local)](http://localhost:8001/docs)
  - **Default Admin User** :
    - **Username** : root
    - **Password** : root
- [Cellarium Backend Production (Local)](http://localhost:8001/docs)
- [Cellarium Backend Production (Heroku)](https://cellarium-backend.herokuapp.com/docs)

## Documentations

Here some documentation :

- [Database](./docs/database.md)
- [Command](./docs/command.md)
- [Idea](./docs/idea.md)

## Requirements

All requirements are in file **/src/requirements.txt** :

- **Infrastructure** :
  - PDM
  - Docker
  - Docker Compose
  - Heroku
  - AWS ?
- **Framework** : FastAPI
- **Security** :
  - PassLib
  - Python Jose
- **Database** :
  - Tortoise ORM
  - Aerich (Migration tool)
- **Database Engine** :
  - SQLite (Driver : AIOSQLite)
  - PostgreSQL (Driver : AsyncPG)
- **Request Engine** :
  - HTTPX (Async HTTP Request Engine)
  - Uvicorn
- **Unit Test** :
  - Pytest
  - Pytest Asyncio (Async Pytest)
  - Anyio (Async Library)
  - Faker (False data generation)
- **Synthax** : Flake8

## Build

### Build : PDM

    cd src
    pdm install

### Build : Docker

    # Development
    docker-compose -f docker-compose.dev.yml build

    # Production
    docker-compose build

## Deploy

### Deploy : PDM

    cd src

    # Database Migration
    pdm run aerich init-db # Init Database (Generate schema and generate app migrate location)
    pdm run aerich upgrade # Upgrade database to lastest version
    pdm run aerich migrate # Generate database migrate changes file

    # Server Start
    pdm run python main.py

### Deploy : Docker

    # Development
    docker-compose -f docker-compose.dev.yml up
    docker-compose -f docker-compose.dev.yml up -d

    # Production
    docker-compose up
    docker-compose up -d

## Unit Testing

### Unit Testing : PDM

    cd src

    # Unit Test
    pdm run pytest tests

    # Synthax Check
    pdm run flake8

### Unit Testing : Docker

    docker-compose -f docker-compose.dev.yml run --rm cellarium_backend_dev pytest tests

## Contributors

- ProgOwer
- TODO

## Licence

This project is licensed under the terms of the GNU General Public Licence v3.0 and above licence.

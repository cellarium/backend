# Import library
from fastapi import APIRouter, Security, HTTPException, Response
from typing import List

from models.brands import schemas
from models.brands import models
from models.brands import crud

from models.users.models import User
from models.security.crud import get_current_active_user


# Create router
router = APIRouter()


# Route definition
@router.get("/", response_model=List[schemas.BrandOut])
async def get_all(
        response: Response,
        skip: int = 0,
        limit: int = None,
        search: str = None,
        order_by: models.SortEnum = models.SortEnum.name,
        order_by_type: models.SortTypeEnum = models.SortTypeEnum.ASC,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Get All Brands.
    """
    filters = schemas.Filters(
        skip=skip,
        limit=limit,
        search=search,
        order_by=order_by,
        order_by_type=order_by_type
    )

    db_data = await crud.get_all(filters=filters)
    response.headers["X-Total-Count"] = str(await crud.get_nb(filters=filters))
    return db_data


@router.get("/{id}", response_model=schemas.BrandOut)
async def get(
        id: int,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Get Brand by ID.
    """
    db_data = await crud.get(id=id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Brand not found")
    return db_data


@router.post("/", response_model=schemas.BrandOut)
async def create(
        brand: schemas.BrandIn,
        current_user: User = Security(get_current_active_user, scopes=["default", "admin"])):
    """
    Create Brand.
    """
    db_data = await crud.get_by_name(brandname=brand.name)
    if db_data is not None:
        raise HTTPException(status_code=404, detail="Brand already exist")
    db_data = await crud.create(brand=brand)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Brand not found")
    return db_data


@router.put("/{id}", response_model=schemas.BrandOut)
async def update(
        id: int,
        brand: schemas.BrandIn,
        current_user: User = Security(get_current_active_user, scopes=["default", "admin"])):
    '''
    Update User.
    '''
    db_data = await crud.get(id=id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Brand not found")
    db_data = await crud.update(id=id, brand=brand)
    return db_data


@router.delete("/{id}", response_model=schemas.BrandOut)
async def delete(
        id: int,
        current_user: User = Security(get_current_active_user, scopes=["default", "admin"])):
    """
    Delete Brand.
    """
    db_data = await crud.get(id=id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Brand not found")
    await crud.delete(id=id)
    return db_data


@router.post("/faker", response_model=List[schemas.BrandOut])
async def faker(
        nb: int,
        current_user: User = Security(get_current_active_user, scopes=["default", "admin"])):
    """
    Generater fake Brands.
    """
    db_data = await crud.faker(nb=nb)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Brands not found")
    return db_data

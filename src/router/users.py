# Import library
from fastapi import APIRouter, Security, HTTPException, Response
from typing import List

from models.users import models
from models.users import schemas
from models.users import crud

from models.security.crud import get_current_active_user
from models.security.crud import verify_password


# Create router
router = APIRouter()


# Route definition
@router.post("/register", response_model=schemas.UserOut)
async def register(user: schemas.UserIn):
    """
    Create User.
    """
    db_data = await crud.get_by_username(username=user.username)
    if db_data is not None:
        raise HTTPException(status_code=404, detail="Username is already used")
    db_data = await crud.create(user=user)
    if db_data is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_data


@router.get("/isUsernameAvailable/", response_model=bool)
async def is_username_available(username: str):
    """
    Check if username is available.
    """
    return await crud.is_username_available(username=username)


@router.get("/isEmailAvailable/", response_model=bool)
async def is_email_available(email: str):
    """
    Check if email is available.
    """
    return await crud.is_email_available(email=email)


@router.get("/my", response_model=schemas.UserOut)
async def get_my_user(current_user: models.User = Security(get_current_active_user, scopes=["default"])):
    return current_user


@router.put("/my", response_model=schemas.UserOut)
async def update_my_user(
        user: schemas.UserBase,
        current_user: models.User = Security(get_current_active_user, scopes=["default"])):
    '''
    Update My User.
    '''
    db_data = await crud.get(id=current_user.id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="User not found")
    db_data = await crud.update(id=current_user.id, user=user)
    return db_data


@router.put("/my/password", response_model=schemas.UserOut)
async def update_my_password(
        password: schemas.UserPassword,
        current_user: models.User = Security(get_current_active_user, scopes=["default"])):
    """
    Update My User Password.
    """
    db_data = await crud.get(id=current_user.id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="User not found")
    if not verify_password(password.old_password, db_data.password):
        raise HTTPException(status_code=401, detail="Bad Password")
    if password.old_password != password.new_password:
        db_data = await crud.update_password(new_password=password.new_password, id=current_user.id)
    return db_data


@router.delete("/my", response_model=schemas.UserOut)
async def delete_my_user(current_user: models.User = Security(get_current_active_user, scopes=["default"])):
    """
    Delete My User.
    """
    db_data = await crud.get(id=current_user.id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="User not found")
    db_data = await crud.delete(id=current_user.id)
    return db_data


@router.get("/", response_model=List[schemas.UserAdminOut])
async def get_all(
        response: Response,
        skip: int = 0,
        limit: int = None,
        search: str = None,
        order_by: models.SortEnum = models.SortEnum.username,
        order_by_type: models.SortTypeEnum = models.SortTypeEnum.ASC,
        current_user: models.User = Security(get_current_active_user, scopes=["default", "admin"])):
    """
    Get All User.
    """
    filters = schemas.Filters(
        skip=skip,
        limit=limit,
        search=search,
        order_by=order_by,
        order_by_type=order_by_type
    )

    db_data = await crud.get_all(filters=filters)
    response.headers["X-Total-Count"] = str(await crud.get_nb(filters=filters))
    return db_data


@router.get("/{id}", response_model=schemas.UserAdminOut)
async def get(
        id: int,
        current_user: models.User = Security(get_current_active_user, scopes=["default", "admin"])):
    """
    Get User by ID.
    """
    db_data = await crud.get(id=id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_data


@router.get("/byUsername/", response_model=schemas.UserAdminOut)
async def get_by_username(
        username: str,
        current_user: models.User = Security(get_current_active_user, scopes=["default", "admin"])):
    """
    Get User by Username.
    """
    db_data = await crud.get_by_username(username=username)
    if db_data is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_data


@router.get("/byEmail/", response_model=schemas.UserAdminOut)
async def get_by_email(
        email: str,
        current_user: models.User = Security(get_current_active_user, scopes=["default", "admin"])):
    """
    Get User by Email.
    """
    db_data = await crud.get_by_email(email=email)
    if db_data is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_data


@router.post("/", response_model=schemas.UserAdminOut)
async def create(
        user: schemas.UserAdminIn,
        current_user: models.User = Security(get_current_active_user, scopes=["default", "admin"])):
    """
    Create User.
    """
    db_data = await crud.get_by_username(username=user.username)
    if db_data is not None:
        raise HTTPException(status_code=404, detail="Username is already used")
    db_data = await crud.create_admin(user=user)
    if db_data is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_data


@router.put("/{id}", response_model=schemas.UserAdminOut)
async def update(
        id: int,
        user: schemas.UserAdminUpdate,
        current_user: models.User = Security(get_current_active_user, scopes=["default", "admin"])):
    '''
    Update User.
    '''
    db_data = await crud.get(id=id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="User not found")
    db_data = await crud.update_admin(id=id, user=user)
    return db_data


@router.put("/{id}/password", response_model=schemas.UserAdminOut)
async def update_password(
        id: int,
        password: schemas.UserAdminPassword,
        current_user: models.User = Security(get_current_active_user, scopes=["default", "admin"])):
    """
    Update User Password.
    """
    db_data = await crud.get(id=id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="User not found")
    db_data = await crud.update_password(new_password=password.new_password, id=id)
    return db_data


@router.delete("/{id}", response_model=schemas.UserAdminOut)
async def delete(
        id: int,
        current_user: models.User = Security(get_current_active_user, scopes=["default", "admin"])):
    """
    Delete User.
    """
    db_data = await crud.get(id=id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="User not found")
    await crud.delete(id=id)
    return db_data

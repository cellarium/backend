# Import library
from fastapi import APIRouter, Security, HTTPException, Response
from typing import List

from models.units import schemas
from models.units import models
from models.units import crud

from models.users.models import User
from models.security.crud import get_current_active_user


# Create router
router = APIRouter()


# Route definition
@router.get("/", response_model=List[schemas.UnitOut])
async def get_all(
        response: Response,
        skip: int = 0,
        limit: int = None,
        search: str = None,
        order_by: models.SortEnum = models.SortEnum.name,
        order_by_type: models.SortTypeEnum = models.SortTypeEnum.ASC,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Get All Units.
    """
    filters = schemas.Filters(
        skip=skip,
        limit=limit,
        search=search,
        order_by=order_by,
        order_by_type=order_by_type
    )

    db_data = await crud.get_all(filters=filters)
    response.headers["X-Total-Count"] = str(await crud.get_nb(filters=filters))
    return db_data


@router.get("/{id}", response_model=schemas.UnitOut)
async def get(
        id: int,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Get Unit by ID.
    """
    db_data = await crud.get(id=id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Unit not found")
    return db_data


@router.post("/", response_model=schemas.UnitOut)
async def create(
        unit: schemas.UnitIn,
        current_user: User = Security(get_current_active_user, scopes=["default", "admin"])):
    """
    Create Unit.
    """
    db_data = await crud.get_by_name(name=unit.name)
    if db_data is not None:
        raise HTTPException(status_code=404, detail="Unit already exist")
    db_data = await crud.create(unit=unit)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Unit not found")
    return db_data


@router.put("/{id}", response_model=schemas.UnitOut)
async def update(
        id: int,
        unit: schemas.UnitIn,
        current_user: User = Security(get_current_active_user, scopes=["default", "admin"])):
    '''
    Update User.
    '''
    db_data = await crud.get(id=id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Unit not found")
    db_data = await crud.update(id=id, unit=unit)
    return db_data


@router.delete("/{id}", response_model=schemas.UnitOut)
async def delete(
        id: int,
        current_user: User = Security(get_current_active_user, scopes=["default", "admin"])):
    """
    Delete Unit.
    """
    db_data = await crud.get(id=id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Unit not found")
    await crud.delete(id=id)
    return db_data


@router.post("/faker", response_model=List[schemas.UnitOut])
async def faker(
        nb: int,
        current_user: User = Security(get_current_active_user, scopes=["default", "admin"])):
    """
    Generater fake Units.
    """
    db_data = await crud.faker(nb=nb)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Units not found")
    return db_data

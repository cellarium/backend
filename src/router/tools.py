# Import library
from fastapi import APIRouter, HTTPException
from typing import Dict


from models.tools import crud


# Create router
router = APIRouter()


@router.get("/root-init", response_model=Dict[str, str])
async def init_root_user():
    """
    Init Root User.
    """
    db_data = await crud.init_root_user()
    if db_data is None:
        raise HTTPException(status_code=404, detail="Root User Init Error !")
    return db_data

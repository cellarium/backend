# Import library
from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm

from datetime import timedelta

from models.security import schemas
from models.security import crud


# Create router
router = APIRouter()


# Route definition
@router.post("/token/username", response_model=schemas.Token)
async def login_for_access_token_with_username(form_data: OAuth2PasswordRequestForm = Depends()):
    # Check User
    user = await crud.authenticate_user_by_username(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )

    # Check Permission
    for permission in form_data.scopes:
        if permission == "admin":
            if not user.is_admin:
                raise HTTPException(
                    status_code=status.HTTP_401_UNAUTHORIZED,
                    detail="Not enough permissions",
                    headers={"WWW-Authenticate": "Bearer"},
                )

    # Create Access Token and Refresh Token
    access_token_expires = timedelta(minutes=crud.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = crud.create_token(
        data={"sub": str(user.id), "scopes": form_data.scopes},
        expires_delta=access_token_expires
    )
    return {
        "access_token": access_token,
        "token_type": "bearer"
    }


@router.post("/token/email", response_model=schemas.Token)
async def login_for_access_token_with_email(form_data: OAuth2PasswordRequestForm = Depends()):
    # Check User
    user = await crud.authenticate_user_by_email(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Bearer"},
        )

    # Check Permission
    for permission in form_data.scopes:
        if permission == "admin":
            if not user.is_admin:
                raise HTTPException(
                    status_code=status.HTTP_401_UNAUTHORIZED,
                    detail="Not enough permissions",
                    headers={"WWW-Authenticate": "Bearer"},
                )

    # Create Access Token and Refresh Token
    access_token_expires = timedelta(minutes=crud.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = crud.create_token(
        data={"sub": str(user.id), "scopes": form_data.scopes},
        expires_delta=access_token_expires
    )
    return {
        "access_token": access_token,
        "token_type": "bearer"
    }

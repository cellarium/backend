# Import library
from fastapi import FastAPI, Depends
from starlette.middleware.cors import CORSMiddleware
from typing import Dict

from app.database import init_db

from app.config import Settings
from app.config import get_settings

# Import Router
from router import tools
from router import security
from router import users
from router import brands
from router import units
from router import sections
from router import categories
from router import items
from router import user_items
from router import shopping_lists
from router import shopping_items
from router import stats


def create_app():
    """
    Create Application FastAPI
    """
    settings = get_settings()

    app_tmp = FastAPI(
        title=settings.title,
        description=settings.description,
        version=settings.version,
        docs_url=settings.docs_url,
        redoc_url=settings.redoc_url
    )

    app_tmp.add_middleware(
        CORSMiddleware,
        allow_origins=['*'],
        allow_credentials=True,
        allow_methods=['*'],
        allow_headers=['*'],
        expose_headers=['X-Total-Count']
    )

    return app_tmp


app = create_app()


# Startup Init
@app.on_event("startup")
async def init_data():
    init_db(app)


# Include Router
app.include_router(
    tools.router,
    tags=['Tools'],
    prefix="/tools"
)

app.include_router(
    security.router,
    tags=['Security Management'],
    prefix="/security"
)

app.include_router(
    users.router,
    tags=['Users Management'],
    prefix="/users"
)

app.include_router(
    brands.router,
    tags=['Brands Management'],
    prefix="/brands"
)

app.include_router(
    units.router,
    tags=['Units Management'],
    prefix="/units"
)

app.include_router(
    sections.router,
    tags=['Sections Management'],
    prefix="/sections"
)

app.include_router(
    categories.router,
    tags=['Categories Management'],
    prefix="/categories"
)

app.include_router(
    items.router,
    tags=['Items Management'],
    prefix="/items"
)

app.include_router(
    user_items.router,
    tags=['User items Management'],
    prefix="/user_items"
)

app.include_router(
    shopping_lists.router,
    tags=['Shopping Lists Management'],
    prefix="/shopping_lists"
)

app.include_router(
    shopping_items.router,
    tags=['Shopping Items Management'],
    prefix="/shopping_items"
)

app.include_router(
    stats.router,
    tags=['Stats Management'],
    prefix="/stats"
)


# Route definition
@app.get("/", response_model=Dict[str, str])
async def information(settings: Settings = Depends(get_settings)):
    """
    Get Information about API
    """
    return {
        "main_documentation": settings.docs_url,
        "recovery_documentation": settings.redoc_url,
        "openapi_config": "/openapi.json",
        "title": settings.title,
        "description": settings.description,
        "version": settings.version
    }

# Import Library
from functools import lru_cache
from os import getenv
from pydantic import BaseSettings


class Settings(BaseSettings):
    """
    Settings Definition
    """
    # Application
    title: str = "Cellarium : Backend"
    description: str = "Cellarium : Backend"
    version: str = "0.39.0"
    docs_url: str = "/docs"
    redoc_url: str = "/redoc"
    env: str = getenv('ENV', 'development')

    # Security Token
    secret_key: str = getenv('SECRET_KEY', 'Undefined')
    algorithm: str = "HS256"
    access_token_expire_minutes: int = 120
    permission_list: dict = {
        1: "default",
        2: "admin"
    }
    permission_description: dict = {
        'default': 'Default Permissions',
        'admin': 'Admin Permissions'
    }

    # Default Admin User
    admin_user: str = getenv('ADMIN_USER', 'root')
    admin_password: str = getenv('ADMIN_PASSWORD', 'root')

    # Database
    database_host: str = getenv('DATABASE_HOST', None)
    database_port: str = getenv('DATABASE_PORT', None)
    database_user: str = getenv('DATABASE_USER', None)
    database_password: str = getenv('DATABASE_PASSWORD', None)
    database_db: str = getenv('DATABASE_DB', None)
    database_sqlite: str = "sqlite://database.db"
    database_test: str = "sqlite://tests/test.db"
    database_models: list = [
        "aerich.models",
        "models.users.models",
        "models.brands.models",
        "models.units.models",
        "models.sections.models",
        "models.categories.models",
        "models.items.models",
        "models.user_items.models",
        "models.shopping_lists.models",
        "models.shopping_items.models",
    ]


@lru_cache()
def get_settings():
    """
    Get Settings Once
    """
    return Settings()

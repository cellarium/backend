# Import library
from pydantic.main import BaseModel
from datetime import date


class UserItemWasteCount(BaseModel):
    '''
    User Item Waste Count Base Model Definition
    '''
    deleted_at: date
    wasted_count: int


class UserItemIO(BaseModel):
    '''
    User Item Input Output Base Model Definition
    '''
    io_date: date
    in_count: int
    out_count: int
    stock_count: int

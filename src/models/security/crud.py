# Import library
from fastapi import Depends, HTTPException, Security, status
from fastapi.security import OAuth2PasswordBearer, SecurityScopes
from typing import Optional
from passlib.context import CryptContext
from jose import JWTError, jwt
from pydantic import ValidationError

from datetime import datetime, timedelta
from secrets import compare_digest

from models.security import schemas

from models.users import crud as usersCrud
from models.users import models as usersModels

from app.config import get_settings


# Get Settings
settings = get_settings()


# Setup Security
SECRET_KEY = settings.secret_key
ALGORITHM = settings.algorithm
ACCESS_TOKEN_EXPIRE_MINUTES = settings.access_token_expire_minutes

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl="security/token/username",
    scopes=settings.permission_description
)


def get_hashed_password(password):
    """
    Get Hashed Password
    """
    return pwd_context.hash(password)


def verify_password(plain_password, hashed_password):
    """
    Verify Password
    """
    return pwd_context.verify(plain_password, hashed_password)


async def authenticate_user_by_username(username: str, password: str):
    """
    Check if User is correct by username
    """
    user = False
    user_tmp = await usersCrud.get_by_username(username=username)
    if user_tmp is not None:
        if user_tmp.is_active:
            if compare_digest(username, user_tmp.username):
                if verify_password(password, user_tmp.password):
                    user = user_tmp
    return user


async def authenticate_user_by_email(email: str, password: str):
    """
    Check if User is correct by email
    """
    user = False
    user_tmp = await usersCrud.get_by_email(email=email)
    if user_tmp is not None:
        if user_tmp.is_active:
            if compare_digest(email, user_tmp.email):
                if verify_password(password, user_tmp.password):
                    user = user_tmp
    return user


def create_token(data: dict, expires_delta: Optional[timedelta] = None):
    """
    Create JWT Access Token
    """
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def get_data(token):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        user_id: str = str(payload.get("sub"))
        if user_id is None:
            raise credentials_exception
        token_scopes = payload.get("scopes", [])
        return {'user_id': user_id, 'scopes': token_scopes}
    except (JWTError, ValidationError):
        raise credentials_exception


async def get_current_user(security_scopes: SecurityScopes, token: str = Depends(oauth2_scheme)):
    """
    Get Current User from Token
    """
    if security_scopes.scopes:
        authenticate_value = f'Bearer scope="{security_scopes.scope_str}"'
    else:
        authenticate_value = "Bearer"

    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )

    try:
        token_data_decoded = get_data(token)
        token_data = schemas.TokenData(
            scopes=token_data_decoded.get('scopes'),
            user_id=token_data_decoded.get('user_id')
        )
    except (JWTError, ValidationError):
        raise credentials_exception

    user = await usersCrud.get(id=token_data.user_id)
    if user is None:
        raise credentials_exception
    for scope in security_scopes.scopes:
        if scope not in token_data.scopes:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Not enough permissions",
                headers={"WWW-Authenticate": authenticate_value},
            )
    return user


async def get_current_active_user(current_user: usersModels.User = Security(get_current_user)):
    """
    Get Current Active User
    """
    if not current_user.is_active:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user

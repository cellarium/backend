# Import library
from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator
from app.database import init_models

from models.users import models


# Init Database Models for relation generation
init_models()


class UserBase(BaseModel):
    '''
    User Base Model Definition
    '''
    username: str
    email: str


class UserIn(UserBase):
    '''
    User Model Definition (For Creation)
    '''
    password: str


class UserAdminIn(UserIn):
    '''
    User Admin Model Definition (For Creation)
    '''
    is_active: bool
    is_admin: bool


class UserAdminUpdate(UserBase):
    '''
    User Admin Model Definition (For Update)
    '''
    is_active: bool
    is_admin: bool


UserOut = pydantic_model_creator(models.User, name="UserOut", exclude=["shopping_lists"])


class UserAdminOut(UserOut):
    '''
    User Admin Model Definition
    '''
    is_active: bool
    is_admin: bool


class UserPassword(BaseModel):
    """
    User Password Change Model Definition
    """
    old_password: str
    new_password: str


class UserAdminPassword(BaseModel):
    """
    User Admin Password Change Model Definition
    """
    new_password: str


class Filters(BaseModel):
    skip: int = 0
    limit: int = None
    search: str = None
    order_by: models.SortEnum = models.SortEnum.username
    order_by_type: models.SortTypeEnum = models.SortTypeEnum.ASC

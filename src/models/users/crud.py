# Import library
from tortoise.query_utils import Q

from models.users import models
from models.users import schemas

from models.security import crud as securityCrud

from app.config import get_settings


async def get_root_user():
    """
    Get Root User.
    """
    settings = get_settings()

    root_user = schemas.UserAdminIn(
        username=settings.admin_user,
        email="admin@mail.net",
        password=settings.admin_password,
        is_active=True,
        is_admin=True
    )

    return root_user


async def get_all(filters: schemas.Filters = schemas.Filters()):
    """
    Get All User.
    """
    filter_args = []

    if filters.search is not None:
        filter_args.append(Q(
            Q(username__icontains=filters.search) | Q(email__icontains=filters.search)))

    if filters.order_by is not None and filters.order_by_type is not None:
        if filters.order_by_type == models.SortTypeEnum.ASC:
            order_by_args = filters.order_by.value
        else:
            order_by_args = "-{}".format(filters.order_by.value)
    else:
        order_by_args = models.SortEnum.username

    db_data = []

    if filters.limit is None:
        db_data = await models.User.filter(*filter_args).order_by(
            order_by_args).offset(filters.skip)
    else:
        db_data = await models.User.filter(*filter_args).order_by(
            order_by_args).offset(filters.skip).limit(filters.limit)

    return db_data


async def get_nb(filters: schemas.Filters = schemas.Filters()):
    """
    Get Nb Users.
    """
    filter_args = []

    if filters.search is not None:
        filter_args.append(Q(
            Q(username__icontains=filters.search) | Q(email__icontains=filters.search)))

    return await models.User.filter(*filter_args).count()


async def get(id: int):
    """
    Get User by ID.
    """
    return await models.User.get_or_none(id=id)


async def get_by_username(username: str):
    """
    Get User by Username.
    """
    return await models.User.get_or_none(username=username)


async def get_by_email(email: str):
    """
    Get User by Email.
    """
    return await models.User.get_or_none(email=email)


async def is_username_available(username: str):
    """
    Check if username is available.
    """
    db_data = await get_by_username(username=username)
    if db_data is None:
        return True
    else:
        return False


async def is_email_available(email: str):
    """
    Check if email is available.
    """
    db_data = await get_by_email(email=email)
    if db_data is None:
        return True
    else:
        return False


async def create(user: schemas.UserIn):
    """
    Create User.
    """
    db_data = user
    db_data.password = securityCrud.get_hashed_password(user.password)
    return await models.User.create(
        username=db_data.username,
        email=db_data.email,
        password=db_data.password
    )


async def create_admin(user: schemas.UserAdminIn):
    """
    Create Admin User.
    """
    db_data = user
    db_data.password = securityCrud.get_hashed_password(user.password)
    return await models.User.create(
        username=db_data.username,
        email=db_data.email,
        password=db_data.password,
        is_active=db_data.is_active,
        is_admin=db_data.is_admin
    )


async def update(id: int, user: schemas.UserBase):
    """
    Update User.
    """
    await models.User.get_or_none(id=id).update(
        username=user.username,
        email=user.email
    )
    return await get(id)


async def update_admin(id: int, user: schemas.UserAdminUpdate):
    """
    Update User.
    """
    await models.User.get_or_none(id=id).update(
        username=user.username,
        email=user.email,
        is_active=user.is_active,
        is_admin=user.is_admin
    )
    return await get(id)


async def update_password(new_password: str, id: int):
    """
    Update User Password.
    """
    await models.User.get_or_none(id=id).update(
        password=securityCrud.get_hashed_password(new_password)
    )
    return await get(id)


async def delete(id: int):
    """
    Delete User.
    """
    db_data = await get(id=id)
    await models.User.filter(id=id).delete()
    return db_data

# Import library
from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator
from typing import List, Dict

from app.database import init_models

from models.shopping_items import models


# Init Database Models for relation generation
init_models()


ShoppingItemIn = pydantic_model_creator(models.ShoppingItem, name="ShoppingItemIn", exclude_readonly=True)
ShoppingItemOut = pydantic_model_creator(models.ShoppingItem, name="ShoppingItemOut")


class ShoppingItemOutFilter(BaseModel):
    shopping_items: List[ShoppingItemOut]
    nb_total_shopping_item: int
    brand_list_available: Dict[str, Dict]
    unit_list_available: Dict[str, Dict]
    section_list_available: Dict[str, Dict]
    category_list_available: Dict[str, Dict]


class SortFilter(BaseModel):
    """
    Sort Filter Definition
    """
    name: models.SortEnum = None
    order: models.SortTypeEnum = None


class ShoppingItemFilter(BaseModel):
    """
    Shopping Item Filter Definition
    """
    skip: int = None
    limit: int = None
    search: str = None
    brand_id_list: List[int] = None
    unit_id_list: List[int] = None
    section_id_list: List[int] = None
    category_id_list: List[int] = None
    check: bool = None
    order_by: List[SortFilter] = None

# Import library
from datetime import date
from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator
from typing import List, Dict

from app.database import init_models

from models.user_items import models


# Init Database Models for relation generation
init_models()


UserItemIn = pydantic_model_creator(models.UserItem, name="UserItemIn", exclude_readonly=True)
UserItemOut = pydantic_model_creator(
    models.UserItem,
    name="UserItemOut",
    exclude=[
        "user.shopping_lists",
        "item.shopping_items"])


class UserItemOutFilter(BaseModel):
    user_items: List[UserItemOut]
    nb_total_user_item: int
    brand_list_available: Dict[str, Dict]
    unit_list_available: Dict[str, Dict]
    section_list_available: Dict[str, Dict]
    category_list_available: Dict[str, Dict]


class SortFilter(BaseModel):
    """
    Sort Filter Definition
    """
    name: models.SortEnum = None
    order: models.SortTypeEnum = None


class UserItemFilter(BaseModel):
    """
    User Item Filter Definition
    """
    skip: int = None
    limit: int = None
    search: str = None
    brand_id_list: List[int] = None
    unit_id_list: List[int] = None
    section_id_list: List[int] = None
    category_id_list: List[int] = None
    wasted: bool = None
    deleted: bool = None
    expire_at: date = None
    order_by: List[SortFilter] = None


class UserItemBase(BaseModel):
    '''
    User Item Base Model Definition
    '''
    quantity: float
    expire_at: date
    deleted_at: date
    wasted: bool


class UserItemCreate(UserItemBase):
    '''
    User Item Update Model Definition
    '''
    item_id: int
    user_id: int

# Import library
from tortoise import fields, models
from enum import Enum


class SortTypeEnum(str, Enum):
    ASC = "ASC"
    DESC = "DESC"


class SortEnum(str, Enum):
    id = "id"
    default_quantity = "default_quantity"
    created_at = "created_at"
    expire_at = "expire_at"
    deleted_at = "deleted_at"
    name = "name"
    brand__name = "brand__name"
    unit__name = "unit__name"
    section__name = "section__name"
    category__name = "category__name"


class SortEnumOld(str, Enum):
    id = "id"
    quantity = "quantity"
    created_at = "created_at"
    expire_at = "expire_at"
    deleted_at = "deleted_at"
    item__name = "item__name"
    item__brand__name = "item__brand__name"
    item__unit__name = "item__unit__name"
    item__section__name = "item__section__name"
    item__category__name = "item__category__name"


class UserItem(models.Model):
    """
    ItemUsers Database Definition
    """

    id = fields.IntField(pk=True)
    quantity = fields.FloatField(null=False)
    created_at = fields.DatetimeField(auto_now_add=True)
    expire_at = fields.DateField(null=True)
    deleted_at = fields.DateField(null=True)
    wasted = fields.BooleanField(default=False, null=False)
    user = fields.ForeignKeyField("models.User", related_name="user_items")
    item = fields.ForeignKeyField("models.Item", related_name="user_items")

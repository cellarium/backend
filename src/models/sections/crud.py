# Import library
from tortoise.query_utils import Q
from faker import Faker

from models.sections import models
from models.sections import schemas


async def get_all(filters: schemas.Filters = schemas.Filters()):
    """
    Get All Sections.
    """
    filter_args = []

    if filters.search is not None:
        filter_args.append(Q(Q(name__icontains=filters.search) | Q(color__icontains=filters.search)))

    if filters.order_by is not None and filters.order_by_type is not None:
        if filters.order_by_type == models.SortTypeEnum.ASC:
            order_by_args = filters.order_by.value
        else:
            order_by_args = "-{}".format(filters.order_by.value)
    else:
        order_by_args = models.SortEnum.name

    db_data = []

    if filters.limit is None:
        db_data = await models.Section.filter(*filter_args).order_by(
            order_by_args).offset(filters.skip)
    else:
        db_data = await models.Section.filter(*filter_args).order_by(
            order_by_args).offset(filters.skip).limit(filters.limit)

    return db_data


async def get_nb(filters: schemas.Filters = schemas.Filters()):
    """
    Get Nb Sections.
    """
    filter_args = []

    if filters.search is not None:
        filter_args.append(Q(Q(name__icontains=filters.search) | Q(color__icontains=filters.search)))

    return await models.Section.filter(*filter_args).count()


async def get(id: int):
    """
    Get Section by ID.
    """
    return await models.Section.get_or_none(id=id)


async def get_by_name(name: str):
    """
    Get Section by name.
    """
    return await models.Section.get_or_none(name=name)


async def create(section: schemas.SectionIn):
    """
    Create Section.
    """
    return await models.Section.create(**section.dict(exclude_unset=True))


async def update(section: schemas.SectionIn, id: int):
    """
    Update Section.
    """
    await models.Section.get_or_none(id=id).update(**section.dict(exclude_unset=True))
    return await get(id=id)


async def delete(id: int):
    """
    Delete Section.
    """
    return await models.Section.get_or_none(id=id).delete()


async def faker(nb: int):
    """
    Generate fake data
    """
    faker = Faker()
    db_data = []
    for i in range(nb):
        db_data.append(await create(section=schemas.SectionIn(
            name=faker.word(),
            color=faker.color()
        )))
    return db_data

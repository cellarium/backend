# Import library
from faker import Faker
from tortoise.query_utils import Prefetch

from models.shopping_lists import models
from models.shopping_lists import schemas

from models.shopping_items import models as shoppingItemsModels

from models.users import models as usersModels


PREFETCH_RELATED = [
    "user",
    "shopping_items__item__brand",
    "shopping_items__item__unit",
    "shopping_items__item__section",
    "shopping_items__item__category",
]


async def get_all_by_user(user: usersModels.User, skip: int = 0, limit: int = None):
    """
    Get All Shopping Lists.
    """
    if limit is None:
        return await models.ShoppingList.filter(user=user).order_by(
            'name').prefetch_related(
                *PREFETCH_RELATED,
                Prefetch(
                    "shopping_items",
                    queryset=shoppingItemsModels.ShoppingItem.all().limit(3)
                )).offset(skip)
    else:
        return await models.ShoppingList.filter(user=user).order_by(
            'name').prefetch_related(
                *PREFETCH_RELATED,
                Prefetch(
                    "shopping_items",
                    queryset=shoppingItemsModels.ShoppingItem.all().limit(3)
                )).offset(skip).limit(limit)


async def get_nb(user: usersModels.User):
    """
    Get Nb Shopping Lists.
    """
    return await models.ShoppingList.filter(
        user=user).prefetch_related(*PREFETCH_RELATED).count()


async def get(id: int):
    """
    Get Shopping List by ID.
    """
    return await models.ShoppingList.get_or_none(id=id).prefetch_related(*PREFETCH_RELATED)


async def create(shopping_list: schemas.ShoppingListIn):
    """
    Create Shopping List.
    """
    db_data = await models.ShoppingList.create(**shopping_list.dict(exclude_unset=True))
    return await get(id=db_data.id)


async def update(shopping_list: schemas.ShoppingListIn, id: int):
    """
    Update Shopping List.
    """
    await models.ShoppingList.get_or_none(id=id).update(**shopping_list.dict(exclude_unset=True))
    return await get(id=id)


async def delete(id: int):
    """
    Delete Shopping List.
    """
    return await models.ShoppingList.get_or_none(id=id).delete()


async def faker(nb: int, user_id: int):
    """
    Generate fake data
    """
    faker = Faker()
    db_data = []
    for i in range(nb):
        db_data.append(await create(shopping_list=schemas.ShoppingListIn(
            name=faker.word(),
            user_id=user_id
        )))
    return db_data

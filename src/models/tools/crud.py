# Import library
from models.users import crud as usersCrud


async def init_root_user():
    """
    Init Root User.
    """
    response = {
        "created": False,
        "updated": False
    }

    root_user = await usersCrud.get_root_user()

    db_data = await usersCrud.get_by_username(username=root_user.username)

    if db_data is not None:
        response['updated'] = True
        await usersCrud.update_admin(user=root_user, id=db_data.id)
        await usersCrud.update_password(new_password=root_user.password, id=db_data.id)
    else:
        response['created'] = True
        db_data = await usersCrud.create_admin(user=root_user)
        if db_data is None:
            response = None

    return response

# Import library
from tortoise.query_utils import Q
from faker import Faker
from random import choice
from random import uniform

from models.items import models
from models.items import schemas
from models.brands import crud as brandCrud
from models.units import crud as unitCrud
from models.sections import crud as sectionCrud
from models.categories import crud as categoryCrud


PREFETCH_RELATED = [
    "brand",
    "unit",
    "section",
    "category"
]


async def get_all(filters: schemas.ItemFilter = schemas.ItemFilter()):
    """
    Get All Items with advanced filters.
    """
    if filters.skip is None:
        filters.skip = 0

    filter_args = []

    if filters.search is not None:
        filter_args.append(Q(name__icontains=filters.search))

    if filters.brand_id_list is None:
        filters.brand_id_list = [item.id for item in await brandCrud.get_all()]
    filter_args.append(Q(brand__id__in=filters.brand_id_list))

    if filters.unit_id_list is None:
        filters.unit_id_list = [item.id for item in await unitCrud.get_all()]
    filter_args.append(Q(unit__id__in=filters.unit_id_list))

    if filters.section_id_list is None:
        filters.section_id_list = [item.id for item in await sectionCrud.get_all()]
    filter_args.append(Q(section__id__in=filters.section_id_list))

    if filters.category_id_list is None:
        filters.category_id_list = [item.id for item in await categoryCrud.get_all()]
    filter_args.append(Q(category__id__in=filters.category_id_list))

    order_by_args = []

    if filters.order_by is not None:
        for order_by in filters.order_by:
            if order_by.order == models.SortTypeEnum.ASC:
                order_by_args.append(order_by.name)
            else:
                order_by_args.append("-{}".format(order_by.name))
    else:
        order_by_args = [models.SortEnum.name]

    db_data = []

    # Get Items with filters
    if filters.limit is None:
        db_data = await models.Item.filter(*filter_args).order_by(
            *order_by_args
        ).prefetch_related(*PREFETCH_RELATED).offset(filters.skip)
    else:
        db_data = await models.Item.filter(*filter_args).order_by(
            *order_by_args
        ).prefetch_related(*PREFETCH_RELATED).offset(filters.skip).limit(filters.limit)

    nbTotal = await models.Item.filter(
        *filter_args).prefetch_related(*PREFETCH_RELATED).count()

    filter_list_tmp = await models.Item.filter(*filter_args).order_by(
        *order_by_args
    ).prefetch_related(*PREFETCH_RELATED).values(
        "brand__id",
        "brand__name",
        "unit__id",
        "unit__name",
        "section__id",
        "section__name",
        "section__color",
        "category__id",
        "category__name",
        "category__color"
    )

    brandListAvailable = {}
    unitListAvailable = {}
    sectionListAvailable = {}
    categoryListAvailable = {}

    for filter in filter_list_tmp:
        if filter["brand__id"] not in brandListAvailable:
            brandListAvailable[filter["brand__id"]] = {"name": filter["brand__name"]}
        if filter["unit__id"] not in unitListAvailable:
            unitListAvailable[filter["unit__id"]] = {"name": filter["unit__name"]}
        if filter["section__id"] not in sectionListAvailable:
            sectionListAvailable[filter["section__id"]] = {
                "name": filter["section__name"],
                "color": filter["section__color"]
            }
        if filter["category__id"] not in categoryListAvailable:
            categoryListAvailable[filter["category__id"]] = {
                "name": filter["category__name"],
                "color": filter["category__color"]
            }

    return schemas.ItemOutFilter(
        items=db_data,
        nb_total_item=nbTotal,
        brand_list_available=brandListAvailable,
        unit_list_available=unitListAvailable,
        section_list_available=sectionListAvailable,
        category_list_available=categoryListAvailable
    )


async def get_all_names():
    """
    Get All Items names
    """
    return await models.Item.all().order_by("name").distinct().values_list("name", flat=True)


async def get(id: int):
    """
    Get Item by ID.
    """
    return await models.Item.get_or_none(id=id).prefetch_related(*PREFETCH_RELATED)


async def get_if_exist(item: schemas.ItemIn):
    """
    Get Item by name, brand, default_quantity, unit.
    """
    return await models.Item.get_or_none(
        name=item.name,
        brand=item.brand_id,
        default_quantity=item.default_quantity,
        unit=item.unit_id
    ).prefetch_related(*PREFETCH_RELATED)


async def create(item: schemas.ItemIn):
    """
    Create Item.
    """
    db_data = await models.Item.create(**item.dict(exclude_unset=True))
    return await get(id=db_data.id)


async def update(item: schemas.ItemIn, id: int):
    """
    Update Item.
    """
    await models.Item.get_or_none(id=id).update(**item.dict(exclude_unset=True))
    return await get(id=id)


async def delete(id: int):
    """
    Delete Item.
    """
    return await models.Item.get_or_none(id=id).delete()


async def faker(nb: int):
    """
    Generate fake data
    """
    faker = Faker()
    brands = await brandCrud.get_all()
    units = await unitCrud.get_all()
    sections = await sectionCrud.get_all()
    categories = await categoryCrud.get_all()
    db_data = []
    for i in range(nb):
        db_data.append(await create(item=schemas.ItemIn(
            name=faker.word(),
            img_path=faker.image_url(),
            brand_id=choice(brands).id,
            default_quantity=uniform(1, 1000),
            unit_id=choice(units).id,
            section_id=choice(sections).id,
            category_id=choice(categories).id
        )))
    return db_data

# Import library
from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator
from typing import List, Dict

from app.database import init_models

from models.items import models


# Init Database Models for relation generation
init_models()


ItemIn = pydantic_model_creator(models.Item, name="ItemIn", exclude_readonly=True)
ItemOut = pydantic_model_creator(models.Item, name="ItemOut", exclude=["shopping_items"])


class ItemOutFilter(BaseModel):
    """
    Item after filter Definition
    """
    items: List[ItemOut]
    nb_total_item: int
    brand_list_available: Dict[str, Dict]
    unit_list_available: Dict[str, Dict]
    section_list_available: Dict[str, Dict]
    category_list_available: Dict[str, Dict]


class SortFilter(BaseModel):
    """
    Sort Filter Definition
    """
    name: models.SortEnum = None
    order: models.SortTypeEnum = None


class ItemFilter(BaseModel):
    """
    Item Filter Definition
    """
    skip: int = None
    limit: int = None
    search: str = None
    brand_id_list: List[int] = None
    unit_id_list: List[int] = None
    section_id_list: List[int] = None
    category_id_list: List[int] = None
    order_by: List[SortFilter] = None

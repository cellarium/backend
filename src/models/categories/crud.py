# Import library
from tortoise.query_utils import Q
from faker import Faker

from models.categories import models
from models.categories import schemas


async def get_all(filters: schemas.Filters = schemas.Filters()):
    """
    Get All Categories.
    """
    filter_args = []

    if filters.search is not None:
        filter_args.append(Q(Q(name__icontains=filters.search) | Q(color__icontains=filters.search)))

    if filters.order_by is not None and filters.order_by_type is not None:
        if filters.order_by_type == models.SortTypeEnum.ASC:
            order_by_args = filters.order_by.value
        else:
            order_by_args = "-{}".format(filters.order_by.value)
    else:
        order_by_args = models.SortEnum.name

    db_data = []

    if filters.limit is None:
        db_data = await models.Category.filter(*filter_args).order_by(
            order_by_args).offset(filters.skip)
    else:
        db_data = await models.Category.filter(*filter_args).order_by(
            order_by_args).offset(filters.skip).limit(filters.limit)

    return db_data


async def get_nb(filters: schemas.Filters = schemas.Filters()):
    """
    Get Nb Categories.
    """
    filter_args = []

    if filters.search is not None:
        filter_args.append(Q(Q(name__icontains=filters.search) | Q(color__icontains=filters.search)))

    return await models.Category.filter(*filter_args).count()


async def get(id: int):
    """
    Get Category by ID.
    """
    return await models.Category.get_or_none(id=id)


async def get_by_name(name: str):
    """
    Get Category by name.
    """
    return await models.Category.get_or_none(name=name)


async def create(category: schemas.CategoryIn):
    """
    Create Category.
    """
    return await models.Category.create(**category.dict(exclude_unset=True))


async def update(category: schemas.CategoryIn, id: int):
    """
    Update Category.
    """
    await models.Category.get_or_none(id=id).update(**category.dict(exclude_unset=True))
    return await get(id=id)


async def delete(id: int):
    """
    Delete Category.
    """
    return await models.Category.get_or_none(id=id).delete()


async def faker(nb: int):
    """
    Generate fake data
    """
    faker = Faker()
    db_data = []
    for i in range(nb):
        db_data.append(await create(category=schemas.CategoryIn(
            name=faker.word(),
            color=faker.color()
        )))
    return db_data

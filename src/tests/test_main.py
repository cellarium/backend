# Import library
import pytest
from httpx import AsyncClient

from app.config import get_settings


# Settings
settings = get_settings()


# Test Index
pytest.global_access_token_admin = ""
pytest.global_access_token_default = ""


@pytest.mark.asyncio
async def test_init_root_user(client: AsyncClient):
    """
    Given nothing,
    When we try to init the root user,
    Then app shall return status 200.
    """
    response = await client.get("/tools/root-init")
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_get_token_admin_by_username(client: AsyncClient):
    """
    Given a username, a password and permissions,
    When we try to login,
    Then app shall return status 200 and a valid token.
    """
    response = await client.post(
        "/security/token/username",
        data={
            "username": settings.admin_user,
            "password": settings.admin_password,
            "scope": "default admin"
        })

    assert response.status_code == 200

    pytest.global_access_token_admin = response.json().get('access_token')


@pytest.mark.asyncio
async def test_get_token_admin_by_email(client: AsyncClient):
    """
    Given a email, a password and permissions,
    When we try to login,
    Then app shall return status 200 and a valid token.
    """
    response = await client.post(
        "/security/token/email",
        data={
            "username": "admin@mail.net",
            "password": settings.admin_password,
            "scope": "default admin"
        })

    assert response.status_code == 200


@pytest.mark.asyncio
async def test_check_token_admin(client: AsyncClient):
    """
    Given a valid admin token,
    When we try get our user,
    Then app shall return status 200.
    """
    response = await client.get(
        "/users/my",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)})
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_get_token_default_by_username(client: AsyncClient):
    """
    Given a username, a password and permissions,
    When we try to login,
    Then app shall return status 200 and a valid token.
    """
    response = await client.post(
        "/security/token/username",
        data={
            "username": settings.admin_user,
            "password": settings.admin_password,
            "scope": "default"
        })

    assert response.status_code == 200

    pytest.global_access_token_default = response.json().get('access_token')


@pytest.mark.asyncio
async def test_check_token_default(client: AsyncClient):
    """
    Given a valid default user token,
    When we try get our user,
    Then app shall return status 200.
    """
    response = await client.get(
        "/users/my",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_get_token_default_by_email(client: AsyncClient):
    """
    Given a email, a password and permissions,
    When we try to login,
    Then app shall return status 200.
    """
    response = await client.post(
        "/security/token/email",
        data={
            "username": "admin@mail.net",
            "password": settings.admin_password,
            "scope": "default"
        })

    assert response.status_code == 200


@pytest.mark.asyncio
async def test_index(client: AsyncClient):
    """
    Given nothing,
    When we try to get information about the API,
    Then app shall return status 200.
    """
    response = await client.get("/")
    assert response.status_code == 200

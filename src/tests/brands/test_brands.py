# Import library
import pytest
from httpx import AsyncClient
from faker import Faker


"""
This file contain the tests related to the brands routes.
"""


# False Data Setup
faker = Faker()


# Test Variable of brand before the update
pytest.brand_before = {
    'id': None,
    'name': faker.word()
}

# Test Variable of brand after the update
pytest.brand_after = {
    'id': None,
    'name': faker.word()
}


# Test Definition
@pytest.mark.asyncio
async def test_create(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /brands/,
    Then app shall return status 200 with the data of the new brand.
    """
    response = await client.post(
        "/brands/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.brand_before.get('name'),
        })
    assert response.status_code == 200

    pytest.brand_before['id'] = response.json().get('id')

    pytest.brand_after['id'] = response.json().get('id')

    assert response.json() == pytest.brand_before


@pytest.mark.asyncio
async def test_get_all_01(client: AsyncClient):
    """
    Given nothing,
    When we execute GET on /brands/ and at least one brand exist,
    Then app shall return status 200 with a list of all the brands.
    """
    response = await client.get(
        "/brands/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 200
    assert pytest.brand_before in response.json()


@pytest.mark.asyncio
async def test_get_one_by_id_01(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /brands/{ID} and the brand exist,
    Then app shall return status 200 with the data of the brand found .
    """
    response = await client.get(
        "/brands/{}".format(pytest.brand_before.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 200
    assert response.json() == pytest.brand_before


@pytest.mark.asyncio
async def test_update_01(client: AsyncClient):
    """
    Given an id with name,
    When we execute PUT on /brands/{ID} and the brand exist,
    Then app shall return status 200 with the data of the brand and update its data in the DB.
    """
    response = await client.put(
        "/brands/{}".format(pytest.brand_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.brand_after.get('name'),
        })
    assert response.status_code == 200
    assert response.json() == pytest.brand_after


@pytest.mark.asyncio
async def test_get_all_02(client: AsyncClient):
    """
    Given nothing,
    When we execute GET on /brands/,
    Then app shall return status 200 with a list of all the brands.
    """
    response = await client.get(
        "/brands/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 200
    assert pytest.brand_after in response.json()


@pytest.mark.asyncio
async def test_get_one_by_id_02(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /brands/{ID},
    Then app shall return status 200 with the data of the brand found.
    """
    response = await client.get(
        "/brands/{}".format(pytest.brand_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 200
    assert response.json() == pytest.brand_after


@pytest.mark.asyncio
async def test_delete(client: AsyncClient):
    """
    Given an ID,
    When we execute DELETE on /brands/{ID} and the brand exist,
    Then app shall return status 200 with the data of the brand and delete the brand in the DB.
    """
    response = await client.delete(
        "/brands/{}".format(pytest.brand_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)})
    assert response.status_code == 200
    assert response.json() == pytest.brand_after


@pytest.mark.asyncio
async def test_get_nothing_by_id_01(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /brands/{ID} and the brand doesn't exist,
    Then app shall return status 404.
    """
    response = await client.get(
        "/brands/{}".format(pytest.brand_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 404

# Import library
import pytest
from httpx import AsyncClient
from faker import Faker
"""
This file contain the tests related to the items filter system routes.
"""


# False Data Setup
faker = Faker()


# Test Variable of item before the update
pytest.items = []
pytest.brands = []
pytest.units = []
pytest.sections = []
pytest.categories = []


# Test Definition
@pytest.mark.asyncio
async def test_create_all(client: AsyncClient):
    """
    Given TODO,
    When TODO,
    Then TODO.
    """
    # Create Brand
    # Create Unit
    # Create Section
    # Create Category
    # Create Item
    pass


# Test Filter System
@pytest.mark.asyncio
async def test_search_filter(client: AsyncClient):
    """
    Given TODO,
    When TODO,
    Then TODO.
    """
    pass


@pytest.mark.asyncio
async def test_list_filter(client: AsyncClient):
    """
    Given TODO,
    When TODO,
    Then TODO.
    """
    pass


@pytest.mark.asyncio
async def test_delete_all(client: AsyncClient):
    """
    Given TODO,
    When TODO,
    Then TODO.
    """
    # Delete Item
    # Delete Brand
    # Delete Unit
    # Delete Section
    # Delete Category
    pass

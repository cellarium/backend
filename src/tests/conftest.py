# Import library
import sys
import os

importPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, importPath + '/../')

import pytest
import asyncio
from tortoise import Tortoise
from typing import Generator
from httpx import AsyncClient

from app.app import app
from app.database import TORTOISE_ORM
from app.config import get_settings


# Unit Test Format, please refer to this to document your test.
"""
Given <some initial context>,
When <this certain type of event occurs>,
Then ensure <this particular outcome>.
"""


# Settings
settings = get_settings()

ROOT_URL = "http://localhost:8001"

TORTOISE_ORM['connections'] = {'default': settings.database_test}


# Setup Test Database
async def init_db(db_url, create_db: bool = False, schemas: bool = False) -> None:
    """
    Initial database connection
    """
    await Tortoise.init(
        db_url=db_url, modules={"models": settings.database_models}, _create_db=create_db
    )
    if create_db:
        print(f"Database created! {db_url = }")
    if schemas:
        await Tortoise.generate_schemas()
        print("Success to generate schemas")


async def init(db_url: str = settings.database_test):
    """
    Init Async database connection
    """
    await init_db(db_url, True, True)


@pytest.fixture(scope="session")
def event_loop():
    """
    No Async System
    """
    yield asyncio.get_event_loop()


@pytest.fixture(scope="session", autouse=True)
async def initialize_tests():
    """
    Init Database and API for Testing
    """
    await init()
    yield
    await Tortoise._drop_databases()


@pytest.fixture(scope="module")
async def client() -> Generator:
    """
    Generator for the Async HTTP Client
    """
    async with AsyncClient(app=app, base_url=ROOT_URL) as ac:
        yield ac

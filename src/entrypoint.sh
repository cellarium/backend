#!/bin/sh

set -e

if [ "$DATABASE_HOST" != "" ]
then
    echo "Waiting for PostGreSQL ..."

    while ! nc -z $DATABASE_HOST $DATABASE_PORT; do
        sleep 0.1
    done

    echo "PostGreSQL started"
fi

aerich init-db
aerich migrate
aerich upgrade

exec "$@"
